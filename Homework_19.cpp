﻿// Homework_19.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>

class Animal
{
public:
    virtual void Voice() const = 0;
    virtual ~Animal() { sayGoodbye(); }

    virtual void sayGoodbye() const { std::cout << "delete animal"; }

};

class Cat : public Animal
{
public:
    void Voice() const override
    {
        std::cout << "Meow\n";
    }

};

class Dog : public Animal
{
public:
    void Voice() const override
    {
        std::cout << "Woof\n";
    }

};

int main()
{
    Animal* animals[2];
    animals[0] = new Cat();
    animals[1] = new Dog();

    for (Animal* a : animals)
    {
        a->Voice();
    }

    delete animals[0], animals[1];
    return EXIT_SUCCESS;

}